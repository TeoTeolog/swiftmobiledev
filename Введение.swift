let name: String = "Litvintsev Alex";
let age: Int = 21;
let currentCity: String = "Saint-Petersburg";
let studyTerm: Float = 3.5;

print("""
     Hi, my name is \(name).
      I'am \(age) years old and I'am living at \(currentCity).
      I am studying at the university and have been studying for \(studyTerm) years now
     """);
