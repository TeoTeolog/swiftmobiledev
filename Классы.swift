class Family {
  var surname = "Ivanov"
};
class Children: Family {
  var quantity = 3
}
class Parents: Family {
  var quantity = 2
}

class House {
  var width = 200;
  var height = 10;
  func create() {
    print("Площадь \(self.height*self.width)");
  }
  func destroy() {
    print("Дом уничтожен")
  }
}

class Pupils {
  var pupils = ["Oleg","Ivan","Andrew"]
  func sortByFirstLetter () {
    print(pupils.sorted())
  }
  func sortByLettersCount() {
    print(self.pupils.sorted(by: >))
  }
}
var students:Pupils = Pupils();
students.sortByFirstLetter();
students.sortByLettersCount();

struct Something {
  var whatever = "whatever";
  func say() {
    print(self.whatever)
  }
}
// Классы отличаются от структур тем, что передаются как ссылочный тип и не имеют поэлементного инициализатора, имеют наследование и деинициализацию
print("---------------")
struct Poker {
  var cards :[(Int, String)] = [];
  func checkCombo() {
    var combos = [String]();
    cards.sorted(by: {($0.0) < ($1.0)})
    for i in 1...4 {
      if (cards[0].1 == cards[i].1) {
        if(i==4) {
          if (cards[0].0 == 10) {
            combos.append("\(cards[0].1) Flesh royal");
            break
          }
          if (cards[4].0 - cards[0].0 == 4) {
            combos.append("\(cards[0].1) strit flesh");
            break
          }
            combos.append("\(cards[0].1) flesh");
        }
      } else {
        var flag = true
        for j in 1...4 {
          if ((cards[0].0)+j != cards[j].0) {
            flag = false
          }
        }
        if (flag) {
            combos.append("strit");
          break;
        }
      }
    }
    if (cards[0].0 == cards[3].0 || cards[4].0 == cards[1].0) {
            combos.append("care"); 
    } else {
      var couple = [String]();
      var triple = [String]();
      var count = 1;
      for j in 1...4 {
        if (cards[j].0 == cards[j-1].0) {
          count+=1;
        } else {
          if (count == 3) {
            triple.append("set")
          } else if ( count == 2) {
            couple.append("couple")
          }
          count = 1;
        }
      }
      if (count == 3) {
            triple.append("set")
          } else if ( count == 2) {
            couple.append("couple")
          }
      if (triple.count == 1 && couple.count == 1) {
            combos.append("Full house"); 
      } else {
        if (triple.count == 1) {
          combos.append(triple[0])
        }
        
        if (couple.count == 2) {
          combos.append("2 couples")
          
        } else {
          for comb in couple {
            combos.append(comb)
          }
        }
      }
    }
    print(combos)
  }
}
var game = Poker(cards: [(7, "diamonds") ,(8, "hearts") , (9, "clubs"), (10, "spades"), (11, "hearts")]);
game.checkCombo()
