enum Enum1: String {
  case case1 = "case1"
  case case2 = "case2"
}
enum Enum2: Int {
  case case1 = 1
  case case2 = 2
}

enum Sex: String {
  case Man, Woman
};
enum Age: Int {
  case Yong = 20
  case Medium = 40
  case Old = 60
};
enum experience: Int {
  case Begginer = 1
  case Middle = 5
  case Experienced = 10
}

enum Rainbow: String {
  case Red, Orange, Yellow, Green, Blue, Indigo, Violet
}

func combineEnums() {
  enum Colors: String {
    case Apple = "green", Mango = "yellow"
  }
  let cases = ["green", "yellow"];
  for color in cases {
    let fruit = Colors(rawValue: color)!
    print("The \(fruit) is \(color)")
  }
}
combineEnums()
print("-------");
enum Score: String {
  case A,B,C,D
}
func grade(score:Score) {
  switch score {
    case .A :
      print ("5")
    case .B:
      print("4")
    case .C:
      print("3")
    case .D: 
    print("2")
  }
}
grade(score:Score.A);
grade(score: Score.D)
print("------")
func getCars() {
  enum AllCars: CaseIterable {
    case Audi, BMW, Porshe
  }
  for car in AllCars.allCases {
    print(car)
  }
}
getCars();