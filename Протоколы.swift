func address(off value: AnyObject) -> String { 
  return "\(Unmanaged.passUnretained(value).toOpaque())"
}
class Ref<T> { 
  var value: T
	
  init(value: T) { 
    self.value = value
  } 
}

struct Box<T> {
  var ref: Ref<T>
	
  init(value: T) { 
    self.ref = Ref(value: value)
  }

	var value: T { 
    get {
      ref.value
    }
    set {
      if (!isKnownUniquelyReferenced(&ref)) {
        ref = Ref(value: newValue) 
      } else {
        ref.value = newValue
      }
    } 
  }
}
struct Test {
  var id = 1
}
let val = Test()

var box = Box(value: val)
var box2 = box

print(address(off: box.ref)) 
print(address(off: box2.ref))
box2.value.id = 12
print(address(off: box.ref)) 
print(address(off: box2.ref))

print("------------");
//----------------------
protocol Hotel {
    var roomCount: Int { get }
}
class HotelAlfa : Hotel {
    let roomCount: Int = 5
}
//----------------------

protocol GameDice  {
    var numberDice : Int { get }
}
extension Int: GameDice {
     var numberDice:Int {
       print("Выпало \(self) на кубике")
        return self
    }
}
let diceCoub = 4;
diceCoub.numberDice
print("----------")

@object protocol OptProtocol {
  var test: String {get}
  @object optional var test2: String {get}
  func someMetod() -> String
}

class TestClass: OptProtocol {
  var test: String
  func someMetod() -> String {
    return test
  }
  init(test: String) {
    self.test = test
  }
}
let result = TestClass(test: "testword");
result.someMetod()
---------------------
protocol StartProtocol {
  var time: Double {get set}
  var codeAmount: Int {get}
  func writeCode()
}

protocol StopProtocol {
  func stopCoding()
}
class Company: StartProtocol, StopProtocol {
  var numberOfSpecialist: Int = 10;
  var platform: String;
  var time: Double
  var codeAmount: Int 
  func writeCode() {
    print("разработка началась. Пишем код на \(platform)")
  }
  func stopCoding() {
    print("Работа закончена")
  }
  init(time: Double, codeAmount: Int,numberOfSpecialist: Int, platform: String) {
    self.time = time
    self.codeAmount = codeAmount
    self.numberOfSpecialist = numberOfSpecialist
    self.platform = platform
  }
}
let company = Company(time: 10.0, codeAmount: 15,numberOfSpecialist: 10, platform: "Android");
company.writeCode();
company.stopCoding()
