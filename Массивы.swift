var daysInMonth: [Int] = [31,28,31,30,31,30,31,31,30,31,30,31];
print(daysInMonth)
print("--------------");
var months = ["Jan","Feb","Mar", "Apr","May","Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
for days in daysInMonth {
  print(days)
}
print("--------------");
for i in 0..<daysInMonth.count{
  print("\(months[i]):\(daysInMonth[i])")
}
print("--------------");
var tuples :[(month: String, days: Int)] = [];
for i in 0...11 {
  tuples.append((month:months[i], days: daysInMonth[i]))
}
for i in tuples {
  print("\(i.0):\(i.1)")
}
print("--------------");
for index in stride(from: 11, through: 0, by: -1) {
  print("\(months[index]):\(daysInMonth[index])")
}
print("--------------");

var month = 8;
var day = 8;
for i in daysInMonth[0..<month-1] {
    day += i
}
print(day)




